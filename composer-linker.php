<?php

// Exit if accessed directly
// defined( 'ABSPATH' ) || exit;

echo "<h1>Welcome on Composer Linker</h1>";

if (!function_exists('composer_get_json_object')) :
    function composer_get_json_object()
    {
        /* Get composer.json file */
        $composerFile = file_get_contents("composer.json");
        return json_decode($composerFile);
    };
endif;

if (!function_exists('composer_get_json_require_object')) :
    function composer_get_json_require_object()
    {
        /* Get composer.json dependency list */
        $composerObj = composer_get_json_object();
        return $composerObj->require;
    };
endif;

if (!function_exists('composer_update_dependency_list_file')) :
    function composer_update_dependency_list_file()
    {
        $dependcyList = [];
        $composerRequireList = composer_get_json_require_object();

        /* Loop over dependencies */
        foreach ($composerRequireList as $composerRequireLabel => $composerRequireVersion) :
            $dependcyList[] = composer_get_dependency_detail_object($composerRequireLabel);
        endforeach;

        /* Write file */
        return !!file_put_contents('composer-linker.json', json_encode($dependcyList, JSON_UNESCAPED_SLASHES));
    };
endif;

if (!function_exists('composer_get_dependency_detail_object')) :
    function composer_get_dependency_detail_object($dependencyName)
    {
        $outputList = [];
        $outputObj = new stdClass();
        $objPreg = '/^([a-z \.]*):(.*)$/';
        $activePreg = '/^(\* )?(.*)$/';

        /* JSON ouput seems buggy so JSON manual conversion */

        exec('composer show "' . $dependencyName . '" --all -f json', $outputList);
        /* Build object */
        foreach ($outputList as $output) :

            $result = [];
            preg_match($objPreg, $output, $result);
            /* If is key value pair (as defined in preg pattern) */
            if (3 === count($result)) :
                /* Extract key and value */
                $label = trim($result[1]);
                $value = trim($result[2]);
                /* Check for specific keys */
                if ('versions' === $label) :
                    /* For version create version list */
                    $versionList = explode(',', $value);
                    foreach ($versionList as $index => $version) :
                        $vResult = [];
                        preg_match($activePreg, trim($version), $vResult);
                        /* Keep only verison number */
                        $versionList[$index] = $vResult[2];
                        /* Set active version */
                        if (!empty($vResult[1])) :
                            $outputObj->active = $vResult[2];
                        endif;
                    endforeach;
                    $value = $versionList;
                endif;
                /* Populate ouput object */
                $outputObj->{$label} = $value;
            endif;

        endforeach;

        return $outputObj;
    };
endif;

// http://mozart.a3web.local/composer-linker.php?action=update&dependency=wpackagist-plugin/bbpress&version=2.6.3

/* Check for URL params */
$action = $_GET['action'];
$dependencyName = $_GET['dependency'];
$depedencyVersion = $_GET['version'];

if (isset($action) && isset($dependencyName) && isset($depedencyVersion)) :
    switch ($action):
        case 'update':
            // This method seems not to work on certain package like "wordpress core"
            // exec('composer require "' . $dependencyName . ':' . $depedencyVersion, $outputList);
            // composer_update_dependency_list_file();
            // echo $dependencyName . ' à été  mis à jour en version ' . $depedencyVersion;

            /* Get composer file */
            $composerObj = composer_get_json_object();
            /* Update / add dependency */
            $composerObj->require->{$dependencyName} = $depedencyVersion;
            /* Overwrite composer file with update version */
            $result = !!file_put_contents('composer.json', json_encode($composerObj, JSON_UNESCAPED_SLASHES));
            $output = [];
            $return_var = 0;
            // var_dump($result);
            if ($result) :
                // sleep(2);
                var_dump(exec('composer update', $output, $return_var));
                // shell_exec('composer update');
                // exec('composer require "' . $dependencyName . ':' . $depedencyVersion, $output, $return_var);
                var_dump($output);
                var_dump($return_var);
                echo $dependencyName . ' à été  mis à jour en version ' . $depedencyVersion;
            endif;

            break;
    endswitch;
else :
    echo "Missing parametters";
endif;
